-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.19-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para procedimiento bd_anselmo.f_insert
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `f_insert`(
	IN `p_nombres` VARCHAR(300),
	IN `p_direc` VARCHAR(400),
	IN `p_correo` VARCHAR(150),
	IN `p_celular` VARCHAR(20),
	IN `p_fecha_nac` TIMESTAMP,
	IN `p_c_1` INT,
	IN `p_c_2` INT,
	IN `p_c_3` INT,
	IN `p_tp_1` INT,
	IN `p_tp_2` INT,
	IN `p_tp_3` INT,
	IN `p_h_1` INT,
	IN `p_h_2` INT,
	IN `p_h_3` INT
)
BEGIN

INSERT INTO usuario
	( nombre_ape, direccion, correo, celular, fecha_nac, c_1, c_2, c_3, tp_1, tp_2, tp_3, h_1, h_2, h_3) 
	VALUES 
	( p_nombres, p_direc, p_correo, p_celular, p_fecha_nac, p_c_1, p_c_2, p_c_3, p_tp_1, p_tp_2, p_tp_3, p_h_1, p_h_2, p_h_3 );
END//
DELIMITER ;

-- Volcando estructura para tabla bd_anselmo.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_ape` varchar(300) NOT NULL,
  `direccion` varchar(400) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `fecha_nac` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `c_1` tinyint(1) NOT NULL DEFAULT '0',
  `c_2` tinyint(1) NOT NULL DEFAULT '0',
  `c_3` tinyint(1) NOT NULL DEFAULT '0',
  `tp_1` tinyint(1) NOT NULL DEFAULT '0',
  `tp_2` tinyint(1) NOT NULL DEFAULT '0',
  `tp_3` tinyint(1) NOT NULL DEFAULT '0',
  `h_1` tinyint(1) NOT NULL DEFAULT '0',
  `h_2` tinyint(1) NOT NULL DEFAULT '0',
  `h_3` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_anselmo.usuario: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id_usuario`, `nombre_ape`, `direccion`, `correo`, `celular`, `fecha_nac`, `c_1`, `c_2`, `c_3`, `tp_1`, `tp_2`, `tp_3`, `h_1`, `h_2`, `h_3`) VALUES
	(1, 'OSCAR FERNANDO DIAZ ANCHAY', 'sdf', 'oscar@gmail.com', '65165156', '0000-00-00 00:00:00', 0, 1, 0, 0, 0, 1, 0, 0, 0),
	(2, 'OSCAR FERNANDO DIAZ ANCHAY', 'asd', 'oscar@gmail.com', '65165156', '2018-08-01 00:00:00', 1, 0, 0, 0, 0, 1, 0, 0, 0);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
