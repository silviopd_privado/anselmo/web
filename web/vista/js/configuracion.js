$(document).ready(function() {

    var x = window.matchMedia("(max-width: 980px)")

    function myFunction(x) {
        if (x.matches) {
            $('#slider_pro').sliderPro({
                arrows: true,
                waitForLayers: true,
                buttons: false,
                autoplay: true,
                width: '99%',
                autoHeight: true,
                autoplayDelay: 3000
            });
        } else {
            $('#slider_pro').sliderPro({
                arrows: true,
                waitForLayers: true,
                buttons: false,
                autoplay: true,
                width: '80%',
                autoHeight: true,
                autoplayDelay: 3000
            });
        }

    }

    myFunction(x)
    x.addListener(myFunction)

    $("#btn-caravas").on('click', function() {
        window.location.href = "./caravanas.html";
    });

    $('.h1-anselmo.header.zoom').click(function(){
        window.location.href = "./index.html";
    });

    var burger = document.getElementById('burger-button');
    var menu = document.getElementById('menu');
    burger.addEventListener('touchstart', function() {
        menu.classList.toggle('active')
    })

    stickyFooter();
});


function stickyFooter() {
    var $body = $(document.body),
        page = $('#body'),
        footer = $('#footer').outerHeight(),
        toolbarHeight;

    $(window).resize(function() {
        toolbarHeight = $body.is('.admin-bar') ? $('#wpadminbar').height() : 0;

        page.css({
            'padding-bottom': footer + toolbarHeight + 'px'
        });
    });
    $(window).trigger('resize');
}