<?php

require_once '../datos/Conexion.clase.php';
require_once '../util/funciones/Funciones.clase.php';

class Usuario extends Conexion {

    private $id_usuario;
    private $nombres;
    private $direccion;
    private $correo;
    private $celular;
    private $fecha_nacimiento;
    private $c_1;
    private $c_2;
    private $c_3;
    private $tp_1;
    private $tp_2;
    private $tp_3;
    private $h_1;
    private $h_2;
    private $h_3;
    private $pp;

    function getId_usuario() {
        return $this->id_usuario;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getCelular() {
        return $this->celular;
    }

    function getFecha_nacimiento() {
        return $this->fecha_nacimiento;
    }

    function getC_1() {
        return $this->c_1;
    }

    function getC_2() {
        return $this->c_2;
    }

    function getC_3() {
        return $this->c_3;
    }

    function getTp_1() {
        return $this->tp_1;
    }

    function getTp_2() {
        return $this->tp_2;
    }

    function getTp_3() {
        return $this->tp_3;
    }

    function getH_1() {
        return $this->h_1;
    }

    function getH_2() {
        return $this->h_2;
    }

    function getH_3() {
        return $this->h_3;
    }

    function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setFecha_nacimiento($fecha_nacimiento) {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    function setC_1($c_1) {
        $this->c_1 = $c_1;
    }

    function setC_2($c_2) {
        $this->c_2 = $c_2;
    }

    function setC_3($c_3) {
        $this->c_3 = $c_3;
    }

    function setTp_1($tp_1) {
        $this->tp_1 = $tp_1;
    }

    function setTp_2($tp_2) {
        $this->tp_2 = $tp_2;
    }

    function setTp_3($tp_3) {
        $this->tp_3 = $tp_3;
    }

    function setH_1($h_1) {
        $this->h_1 = $h_1;
    }

    function setH_2($h_2) {
        $this->h_2 = $h_2;
    }

    function setH_3($h_3) {
        $this->h_3 = $h_3;
    }
    
    function getPp() {
        return $this->pp;
    }

    function setPp($pp) {
        $this->pp = $pp;
    }

    public function agregar() {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL f_insert(UPPER(:p_nombres), :p_direc,:p_correo,:p_cel,:p_fecha_nac,:p_c_1,:p_c_2,:p_c_3,:p_tp_1,:p_tp_2,:p_tp_3,:p_h_1,:p_h_2,:p_h_3,:p_pp  );";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombres", $this->getNombres());
            $sentencia->bindValue(":p_direc", $this->getDireccion());
            $sentencia->bindValue(":p_correo", $this->getCorreo());
            $sentencia->bindValue(":p_cel", $this->getCelular());
            $sentencia->bindValue(":p_fecha_nac", $this->getFecha_nacimiento());
            $sentencia->bindValue(":p_c_1", $this->getC_1());
            $sentencia->bindValue(":p_c_2", $this->getC_2());
            $sentencia->bindValue(":p_c_3", $this->getC_3());
            $sentencia->bindValue(":p_tp_1", $this->getTp_1());
            $sentencia->bindValue(":p_tp_2", $this->getTp_2());
            $sentencia->bindValue(":p_tp_3", $this->getTp_3());
            $sentencia->bindValue(":p_h_1", $this->getH_1());
            $sentencia->bindValue(":p_h_2", $this->getH_2());
            $sentencia->bindValue(":p_h_3", $this->getH_3());
            $sentencia->bindValue(":p_pp", $this->getPp());

            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

}
