-- --------------------------------------------------------
-- Host:                         67.225.178.155
-- Versión del servidor:         5.6.39 - MySQL Community Server (GPL)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             9.5.0.5284
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para procedimiento wildergu_bd_anselmo.f_insert
DELIMITER //
CREATE DEFINER=`wildergu_ansel`@`%` PROCEDURE `f_insert`(
	IN `p_nombres` VARCHAR(300),
	IN `p_direc` VARCHAR(400),
	IN `p_correo` VARCHAR(150),
	IN `p_celular` VARCHAR(20),
	IN `p_fecha_nac` TIMESTAMP,
	IN `p_c_1` INT,
	IN `p_c_2` INT,
	IN `p_c_3` INT,
	IN `p_tp_1` INT,
	IN `p_tp_2` INT,
	IN `p_tp_3` INT,
	IN `p_h_1` INT,
	IN `p_h_2` INT,
	IN `p_h_3` INT
,
	IN `pp` INT
)
BEGIN

INSERT INTO usuario
	( nombre_ape, direccion, correo, celular, fecha_nac, c_1, c_2, c_3, tp_1, tp_2, tp_3, h_1, h_2, h_3,pp) 
	VALUES 
	( p_nombres, p_direc, p_correo, p_celular, p_fecha_nac, p_c_1, p_c_2, p_c_3, p_tp_1, p_tp_2, p_tp_3, p_h_1, p_h_2, p_h_3,pp );
END//
DELIMITER ;

-- Volcando estructura para tabla wildergu_bd_anselmo.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_ape` varchar(300) NOT NULL,
  `direccion` varchar(400) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `fecha_nac` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `c_1` tinyint(1) NOT NULL DEFAULT '0',
  `c_2` tinyint(1) NOT NULL DEFAULT '0',
  `c_3` tinyint(1) NOT NULL DEFAULT '0',
  `tp_1` tinyint(1) NOT NULL DEFAULT '0',
  `tp_2` tinyint(1) NOT NULL DEFAULT '0',
  `tp_3` tinyint(1) NOT NULL DEFAULT '0',
  `h_1` tinyint(1) NOT NULL DEFAULT '0',
  `h_2` tinyint(1) NOT NULL DEFAULT '0',
  `h_3` tinyint(1) NOT NULL DEFAULT '0',
  `pp` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla wildergu_bd_anselmo.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
