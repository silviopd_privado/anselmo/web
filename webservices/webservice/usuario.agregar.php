<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Usuario.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$nombre = $_POST["nombre"];
$direc = $_POST["direc"];
$correo = $_POST["correo"];
$cel = $_POST["cel"];
$fecha_nac = $_POST["fecha_nac"];

$c_1 = $_POST["c_1"];
$c_2 = $_POST["c_2"];
$c_3 = $_POST["c_3"];
$tp_1 = $_POST["tp_1"];
$tp_2 = $_POST["tp_2"];
$tp_3 = $_POST["tp_3"];
$h_1 = $_POST["h_1"];
$h_2 = $_POST["h_2"];
$h_3 = $_POST["h_3"];
$pp = $_POST["pp"];

try {

    $obj = new Usuario();
    $obj->setNombres($nombre);
    $obj->setDireccion($direc);
    $obj->setCorreo($correo);
    $obj->setCelular($cel);
    $obj->setFecha_nacimiento($fecha_nac);

    $obj->setC_1($c_1);
    $obj->setC_2($c_2);
    $obj->setC_3($c_3);
    $obj->setTp_1($tp_1);
    $obj->setTp_2($tp_2);
    $obj->setTp_3($tp_3);
    $obj->setH_1($h_1);
    $obj->setH_2($h_2);
    $obj->setH_3($h_3);
    $obj->setPp($pp);


    $resultado = $obj->agregar();

    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
